function Menu(){
  this.setup = function(){
    startButton = new Button(70,25,"S T A R T");
    $("#movement-instruction").css("display", "block");
  }

  this.draw = function(){
    noStroke();
    fill(color(50,50,50));
    rect(0,0,width,height);
    startButton.draw();

    textSize(70);
    fill(color(245,245,245));
    textAlign(CENTER);
    text(difficultyName, width/2, 100);

    textSize(18);
    fill(color(245,245,245));
    noStroke();
    textAlign(CENTER);
    text("( S P A C E B A R )  O R  ( E N T E R )", width/2, 300);
    //map.end();
  }
}
