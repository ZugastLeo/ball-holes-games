function CrownHat(ball){
  this.ball = ball;

  this.draw = function(){
    var x = this.ball.x;
    var y = this.ball.y;

    fill("#FFFF00");
    rect(x-19, y-15, 37, 5);
    triangle(x-18, y-15, x-20, y-35, x-5, y-15);
    triangle(x-10, y-15, x, y-35, x+10, y-15);
    triangle(x+5, y-15, x+20, y-35, x+18, y-15);
  }
}
