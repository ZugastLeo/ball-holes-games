$(function(){
  $("#movement-instruction").css("transition", "border-color .3s, height .2s");
  $("#movement-instruction div").css("transition", "background-color .3s");
  $(".difficulty-button").css("transition", "background-color .3s");
  $("#game-container ").css("transition", "border-color.3s");
  $(".sliding-panel-left").css("transition", "border-color .3s, left .3s");
  $(".sliding-panel-button-left").css("transition", "border-color .3s, background-color .3s");
  $(".sliding-panel-right").css("transition", "border-color .3s, right .3s");
  $(".sliding-panel-button-right").css("transition", "border-color .3s, background-color .3s");

  difficulty = 100;
  difficultyName = "N O R M A L";

  $('button').focus(function() {
       this.blur();
  });
  expand();

  if (localStorage.getItem("easy")){
    $("#" + "easy" + " span").html(localStorage.getItem("easy"));
  }
  document.getElementById("easy").onclick = easy;

  if (localStorage.getItem("normal")){
    $("#" + "normal" + " span").html(localStorage.getItem("normal"));
  }
  document.getElementById("normal").onclick = normal;

  if (localStorage.getItem("hard")){
    $("#" + "hard").removeClass("locked");
    $("#" + "hard" + " p").html("HARD");
    $("#" + "hard" + " span").html(localStorage.getItem("hard"));
    document.getElementById("hard").onclick = hard;
  }
  if (localStorage.getItem("extreme")){
    $("#" + "extreme").removeClass("locked");
    $("#" + "extreme" + " p").html("EXTREME");
    $("#" + "extreme" + " span").html(localStorage.getItem("extreme"));
    document.getElementById("extreme").onclick = extreme;
  }

  $("#store").click(clickStoreItem);

  makeCoolText("#game h1", "GALACTIC SNAKE");
});

function easy(){
  end();
  difficulty = 250;
  difficultyName = "E A S Y";
}

function normal(){
  end();
  difficulty = 100;
  difficultyName = "N O R M A L";
}

function hard(){
  end();
  difficulty = 70;
  difficultyName = "H A R D";
}

function extreme(){
  end();
  difficulty = 30;
  difficultyName = "E X T R E M E";
}

var controlOut = false;
function controls(){
  $("#movement-instruction").css("height", controlOut ? "0px" : "100px");
  $("#controls-arrow-left").css("transform", controlOut ? "rotate(-90deg)" : "rotate(90deg)");
  $("#controls-arrow-right").css("transform", controlOut ? "rotate(-90deg)" : "rotate(90deg)");
  controlOut = !controlOut;
}

var rightOut = false;
function instructions(){
  $(".sliding-panel-right").css("right", rightOut ? "0px" : "-300px");
  $("#instructions-arrow-top").css("transform", rightOut ? "rotate(0deg)" : "rotate(180deg)");
  $("#instructions-arrow-bottom").css("transform", rightOut ? "rotate(0deg)" : "rotate(180deg)");
  rightOut = !rightOut;
}

var leftOut = false;
function levels(){
  $(".sliding-panel-left").css("left", leftOut ? "0px" : "-300px");
  $("#levels-arrow-top").css("transform", leftOut ? "rotate(180deg)" : "rotate(0deg)");
  $("#levels-arrow-bottom").css("transform", leftOut ? "rotate(180deg)" : "rotate(0deg)");
  leftOut = !leftOut;
}

function expand(){
  $("#movement-instruction").css("transition", "height 1.5s");
  $("#controls-arrow-left").css("transition", "left 1s");
  $("#controls-arrow-right").css("transition", "right 1.5s");
  levels();
  instructions();
  controls()
  $("#movement-instruction").css("transition", "height .3s");
  $("#controls-arrow-left").css("transition", "left .2s");
  $("#controls-arrow-right").css("transition", "right .3s");
}

function disableButtons(){
  /*document.getElementById("practice").disabled=true;
  document.getElementById("easy").disabled=true;
  document.getElementById("normal").disabled=true;
  document.getElementById("hard").disabled=true;*/
}

function enableButtons(){
  /*document.getElementById("practice").disabled=false;
  document.getElementById("easy").disabled=false;
  document.getElementById("normal").disabled=false;
  document.getElementById("hard").disabled=false;*/
}

function toRadians(angle) {
  return angle * Math.PI/180;
}
