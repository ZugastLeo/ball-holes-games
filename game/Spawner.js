function Spawner(x,y,t){
  this.x = x;
  this.y = y;
  this.t = t;

  this.draw = function(){
    this.timer.tick();
  }

  this.setup = function(spawner){
    spawner.ms = Math.floor(Math.random() * spawner.t+10);
    spawner.timer = new Timer(function(){spawner.spawn(spawner);}, 10*spawner.ms);
  }

  this.spawn = function(spawner){
    var isCoin = (Math.random() * 10 - 8.5) >= 0;
    map.holes.push(isCoin ? new Coin(spawner.x, spawner.y) : new Hole(spawner.x, spawner.y));
    spawner.setup(spawner);
  }
}
