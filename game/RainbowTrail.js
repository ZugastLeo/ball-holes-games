function RainbowTrail(ball){
  this.trail = [];
  this.ball = ball;

  var center = 128;
  var width = 127;
  var phase = 100;

  this.draw = function(){
    var frequency = Math.PI*2/this.trail.length;

    var r;
    var g;
    var b;

    for(var i = 0; i < this.trail.length; i++){
      var radius = this.ball.radius * (1 - 0.01 * (this.trail.length - i));
      if(radius <= this.ball.radius * 0.15){
        this.trail.splice(i, 1);
      } else {
        noStroke()
        r = Math.sin(frequency*i+2+phase) * width + center;
        g = Math.sin(frequency*i+0+phase) * width + center;
        b = Math.sin(frequency*i+4+phase) * width + center;
        fill(color(r, g, b));
        ellipse(this.trail[i][0], this.trail[i][1] + (this.trail.length - i), 2*radius, 2*radius);
      }
    }
    var c = color(r, g, b);
    fill(c);
    stroke(color(200,200,200));
    strokeWeight(1);
    ellipse(this.ball.x,this.ball.y,this.ball.radius*2,this.ball.radius*2);

    phase+=0.02;

    this.trail.push([this.ball.x, this.ball.y]);
  }
}
