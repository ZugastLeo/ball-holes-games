/*
bar object
represents the horizontal bar that allows the player to
move the ball arround the board

y1: height of left side
y2: height of right side
*/
function Bar(y1, y2){
  this.x1 = 0;
  this.y1 = y1;
  this.x2 = width;
  this.y2 = y2;

  this.tr = [15, 17, 22, 10, 8, 10, color(255,255,255)];
  this.br = [15, 17, 22, 10, 8, 10, color(255,255,255)];
  this.tl = [15, 17, 22, 10, 8, 10, color(255,255,255)];
  this.bl = [15, 17, 22, 10, 8, 10, color(255,255,255)];
  /*
  draw method runs every frame
  handles movements and then drawing on screen
  */
  this.draw = function(){
    this.tilt();

    strokeWeight(4);
    stroke(color(255,255,255));
    line(this.x1, this.y1, this.x2, this.y2);

    noStroke();
    fill(this.tr[6]);
    triangle(this.x2-this.tr[0], this.y2-this.tr[1], this.x2-this.tr[2], this.y2-this.tr[3], this.x2-this.tr[4], this.y2-this.tr[5]);

    fill(this.br[6]);
    triangle(this.x2-this.br[0], this.y2+this.br[1], this.x2-this.br[2], this.y2+this.br[3], this.x2-this.br[4], this.y2+this.br[5]);

    fill(this.tl[6]);
    triangle(this.x1+this.tl[0], this.y1-this.tl[1], this.x1+this.tl[2], this.y1-this.tl[3], this.x1+this.tl[4], this.y1-this.tl[5]);

    fill(this.bl[6]);
    triangle(this.x1+this.bl[0], this.y1+this.bl[1], this.x1+this.bl[2], this.y1+this.bl[3], this.x1+this.bl[4], this.y1+this.bl[5]);
    //triangle(this.x1, this.y1, x2, y2, x3, y3)

    //triangle(this.x2, this.y2, x2, y2, x3, y3)
  }

  /*
  returns the slope of the line represented by the bar
  slope = (y2-y1)/(x2-x1)
  */
  this.m = function(){
    return (this.y2 - this.y1)/(this.x2 - this.x1);
  }

  /*
  returns value of the linear function representing the line
  at point x

  x: x value of the desired point
  */
  this.f = function(x){
    return this.m() * (x - this.x1) + this.y1;
  }

  /*
  returns true if the ball is above the Bar
  returns false if the ball is below

  ball: ball object
  */
  this.aboveBar = function(){
    if(ball.y + ball.radius < this.f(ball.x)){
      return true;
    } else {
      return false;
    }
  }

  /*
  tilts the bar according to key presses
  */
  this.tilt = function(){
    this.tl = [15, 17, 22, 10, 8, 10, color(255,255,255)];
    this.bl = [15, 17, 22, 10, 8, 10, color(255,255,255)];
    if(keyIsDown(65)){
      this.y1 += -5;
      if(this.y1 < 0){
        this.y1 = 0;
      }

      this.tl = [15, 21, 26, 10, 4, 10, color(255,0,0)/*color(16,112,209)*/];
    } else if (keyIsDown(90)){
      this.y1 += 5;
      if(this.y1 > height){
        this.y1 = height;
      }

      this.bl = [15, 21, 26, 10, 4, 10, color(255,0,0)];
    }

    this.tr = [15, 17, 22, 10, 8, 10, color(255,255,255)];
    this.br = [15, 17, 22, 10, 8, 10, color(255,255,255)];
    if (keyIsDown(75)){
      this.y2 += -5;
      if(this.y2 < 0){
        this.y2 = 0;
      }

      this.tr = [15, 21, 26, 10, 4, 10, color(255,0,0)];
    } else if (keyIsDown(77)){
      this.y2 += 5;
      if(this.y2 > height){
        this.y2 = height;
      }
      this.br = [15, 21, 26, 10, 4, 10, color(255,0,0)];
    }
  }
}
