var holes = [];

function setup() {
  createCanvas(500, 500);
}

function draw() {
  //draw background (color)
  background(222,184,135);
  for(var i = 0; i < holes.length; i++){
    holes[i].draw();
  }
}

function keyPressed(){
  if (keyCode === LEFT_ARROW) {
    holes.push(new Hole(mouseX, mouseY));
  } else if (keyCode === RIGHT_ARROW) {
    var map = [];
    for(var i = 0; i < holes.length; i++){
      map.push([holes[i].x, holes[i].y]);
    }
  }
}
