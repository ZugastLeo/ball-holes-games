function BlueTrail(ball){
  this.trail = [];
  this.ball = ball;

  this.r1 = 170;
  this.g1 = 25;
  this.b1 = 255;

  this.r2 = 24;
  this.g2 = 105;
  this.b2 = 255;

  this.draw = function(){
    for(var i = 0; i < this.trail.length; i++){
      var radius = this.ball.radius * (1 - 0.01 * (this.trail.length - i));
      if(radius <= this.ball.radius * 0.15){
        this.trail.splice(i, 1);
      } else {
        noStroke()
        var r = this.r2 + (this.r1 - this.r2) * (i/this.trail.length);
        var g = this.g2 + (this.g1 - this.g2) * (i/this.trail.length);
        var b = this.b2 + (this.b1 - this.b2) * (i/this.trail.length);
        fill(color(r, g, b));
        ellipse(this.trail[i][0], this.trail[i][1] + (this.trail.length - i), 2*radius, 2*radius);
      }
    }
    var c = color(this.r1, this.g1, this.b1);
    fill(c);
    stroke(color(200,200,200));
    strokeWeight(1);
    ellipse(this.ball.x,this.ball.y,this.ball.radius*2,this.ball.radius*2);

    this.trail.push([this.ball.x, this.ball.y]);
  }
}
