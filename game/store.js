//When an item is locked it displays the cost and is colored grey
//When an item is unlocked it displays available and is colored in a light blue
//When an item is selected it displays selected and is colored blue

//on the first load:
//orange-trail should be unlocked and Selected
//everything else should be locked

//when purchasing an item
//total money is reduced
//item is unlocked and then selected

//when selecting an item
//its just selected... what did you expect

/*
this variable contains the current data for the store
for each button:
1. contains the constructor for the item in game
2. contains whether it has previously been unlocked
3. contains whether it is currently selected
4. contains the price of the item

initialized to default values
*/
var store = {
    //Trails
    "orange-trail" : {
      fcn: RedTrail,
      state: "selected",
      price: 0,
      type: "trail"
    },

    "green-trail" : {
      fcn: GreenTrail,
      state: "locked",
      price: 5,
      type: "trail"
    },

    "purple-trail" : {
      fcn: BlueTrail,
      state: "locked",
      price: 10,
      type: "trail"
    },

    "rainbow-trail" : {
      fcn: RainbowTrail,
      state: "locked",
      price: 20,
      type: "trail"
    },

    //Hats
    "no-hat" : {
      fcn: NoHat,
      state: "selected",
      price: 0,
      type: "hat"
    },

    "angel-hat" : {
      fcn: AngelHat,
      state: "locked",
      price: 5,
      type: "hat"
    },

    "cowboy-hat" : {
      fcn: CowboyHat,
      state: "locked",
      price: 10,
      type: "hat"
    },

    "crown-hat" : {
      fcn: CrownHat,
      state: "locked",
      price: 20,
      type: "hat"
    }
}

/*
this function must takes care of loading from localStorage what
items have previously been purchased and which items are
currently selected by the player
*/
function storeLoad(){
  fakeStore = JSON.parse(localStorage.getItem("store"));
  if(fakeStore){
    for(const itemId in store){
      store[itemId].state = fakeStore[itemId].state;
    }
  }
}

/*
this function must takes care of loading from localStorage what
items have previously been purchased and which items are
currently selected by the player
*/
function storeSave(){
  localStorage.setItem("store", JSON.stringify(store));
}

/*
this function must takes care of loading from localStorage what
items have previously been purchased and which items are
currently selected by the player
*/
function storeDraw(){
  function stateLoad(itemId){
    $("#" + itemId).removeClass("locked");
    $("#" + itemId).removeClass("unlocked");
    $("#" + itemId).removeClass("selected");
    switch(store[itemId].state){
      case "locked":
        $("#" + itemId).addClass("locked");
        $("#" + itemId + " .price").html(store[itemId].price);
        break;
      case "unlocked":
        $("#" + itemId).addClass("unlocked");
        $("#" + itemId + " .price").html(" - ");
        break;
      case "selected":
        $("#" + itemId).addClass("selected");
        $("#" + itemId + " .price").html(" ✔ ");
        if(("" + itemId).includes("trail")){
          currentTrail = store[itemId].fcn;
        } else {
          currentHat = store[itemId].fcn;
        }
        break;
    }
  }

  for(const itemId in store){
    stateLoad(itemId);
  }
}

/*
event function
handles any click on any button in the store
this function is attached to the click event on the store
*/
function clickStoreItem(e){
  e = e || window.event;
  e = e.target || e.srcElement;
  classes = e.classList;
  if (e.nodeName === "BUTTON") {
    if (isThisTypeOfItem(classes, "trail")){
      if(store[e.id].state != "locked"){
        changeItem(e.id, "trail");
      } else {
        var successful = purchaseItem(e.id);
        if(successful){
          changeItem(e.id, "trail");
        }
      }
    } else if (isThisTypeOfItem(classes, "hat")){
        if(store[e.id].state != "locked"){
          changeItem(e.id, "hat");
        } else {
          var successful = purchaseItem(e.id);
          if(successful){
            changeItem(e.id, "hat");
          }
        }
    } 
  }
  saveCoins();
  storeSave();
  storeLoad();
  storeDraw();
}

/*
Handles the purchase of an Item
1. reduces the money of the player by the cost
2. selects the item
3. unlocks the item and updates localStorage
*/
function purchaseItem(itemId){
  if(store[itemId].price > money){
    notEnoughMoney();
    return false;
  } else {
    money-=store[itemId].price;
    changeItemState(itemId, 2);
    return true;
  }
}

/*
alerts the player that they do not have enough money for this item
Must be improved cannot just be an alert!!!!!!!!!!
*/
function notEnoughMoney(){
  alert("You do not have enough money for this item");
}


//changes which item is selected
function changeItem(itemId, itemType){
  for(const item in store){
    if(store[item].state == "selected" && store[item].type == itemType){
      store[item].state = "unlocked";
    }
  }
  store[itemId].state = "selected";
}

/*
  Changes the state of the button of an item in the store
  this will change the message (price, or unlocked/selected)
  and it will also change the background color

  state can be:
  "locked"
  "unlocked"
  "selected"
*/
function changeItemState(itemId, state){
  store[itemId].state = state;
}

function isThisTypeOfItem(classes, itemType){
  for(var i = 0; i < classes.length; i++){
    if(classes[i] == itemType + "-button"){
      return true;
    }
  }
  return false
}
