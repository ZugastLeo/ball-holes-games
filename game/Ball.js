/*
ball object
represents the ball in the game

x: horizontal position
y: vertical position
xSpeed: horizontal speed
ySpeed: vertical speed
*/
function Ball(x, y, trail, hat){
  this.x = x;
  this.y = y;
  this.xSpeed = 0;
  this.ySpeed = 0;
  this.radius = 20;
  this.trail = new trail(this);
  this.hat = new hat(this);

  /*
  draw method runs every frame
  handles movements and then drawing on screen
  */
  this.draw = function(){
    this.fall();
    this.sideMove();
    this.trail.draw();
    this.hat.draw();
  }

  /*
  handles the effect of gravity on the ball
  */
  this.fall = function(){
    this.y += this.ySpeed;
    this.ySpeed += gravity;
    if (!bar.aboveBar(this)){
      this.y = bar.f(this.x) - this.radius;
    }
  }

  /*
  handles horizontal movement according to gravity and tilt of Bar
  */
  this.sideMove = function(){
    this.x += this.xSpeed;
    var theta = Math.atan(bar.m());
    this.xSpeed += gravity * Math.sin(theta);

    if (this.x < bar.x1 + this.radius){
      this.x = bar.x1 + this.radius;
      this.xSpeed = 0;
    } else if (this.x > bar.x2 - this.radius){
      this.x = bar.x2 - this.radius;
      this.xSpeed = 0;
    }
  }

  /*
  makes the ball jump
  */
  this.jump = function(){
    this.ySpeed = -20;
  }
}
