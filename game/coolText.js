function makeCoolText(elementIdentifier, message){
  $(elementIdentifier).lettering();
  var listOfRandomChars = [':', ';', '}', '{', '(', ')', '[', ']', '?', '!', '>', '<', '@', '#', '$', '%', '&'];
  var currentMessage =  "";
  for(var i = 0; i < message.length; i++){
    var randomChar = Math.floor(Math.random() * listOfRandomChars.length);
    currentMessage += listOfRandomChars[randomChar];
  }

  var currentIndex = 0;
  var switchNumber = 0;
  var maxSwitch = 5;
  var interval = setInterval(function(){
    var randomChar2 = Math.floor(Math.random() * listOfRandomChars.length);
    currentMessage = currentMessage.replaceAt(currentIndex, listOfRandomChars[randomChar2]);
    switchNumber++;
    if(switchNumber == maxSwitch){
      currentMessage = currentMessage.replaceAt(currentIndex, message.charAt(currentIndex));
      switchNumber = 0;
      currentIndex++;
      if(currentIndex == message.length){
        clearInterval(interval);
      }
    }
    $(elementIdentifier).html(currentMessage);
    $(elementIdentifier).lettering();
  }, 50);
}

String.prototype.replaceAt=function(index, replacement) {
    return this.substr(0, index) + replacement+ this.substr(index + replacement.length);
}
