function CowboyHat(ball){
  this.ball = ball;

  this.draw = function(){
    var x = this.ball.x;
    var y = this.ball.y;

    noFill();
    stroke("#966a04");
    strokeWeight(5);
    arc(x, y-15, 40, 5, 0, PI);
    line(x-10, y-15, x-5, y-30);
    line(x+10, y-15, x+5, y-30);
    arc(x+.5, y-29.5, 10, 5, 0, PI);

    line(x-5, y-15, x-3, y-25);
    line(x+5, y-15, x+3, y-25);
    line(x-2, y-15, x, y-25);
    line(x+2, y-15, x, y-25);

    stroke("#4c3500");
    arc(x+.5, y-18, 19, 1, 0, PI);

    fill("#4c3500");
    strokeWeight(2);
    line(x-10.6, y-16.5, x-9.6, y-19.5);
    line(x+11.8, y-16.5, x+10.8, y-19.5);
    line(x+11.9, y-16.5, x-10.7, y-16.5);
  }
}
