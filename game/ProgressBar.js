function ProgressBar(){
  this.s = 15;

  this.x1 = 0;
  this.y1 = 0;
  this.x2 = width;
  this.y2 = 22;

  this.px1 = 0;
  this.py1 = 0;
  this.px2 = 0;
  this.py2 = 22;


  this.draw = function(){
    noStroke();

    fill(color(65, 65, 65));
    rect(this.x1, this.y1, this.x2, this.y2);

    fill(map.hred, map.hgreen, map.hblue);
    rect(this.px1, this.py1, this.px2, this.py2);

    noStroke();
    textSize(20);
    textStyle(BOLD);
    fill(color(245,245,245));
    textAlign(CENTER);
    text("S T A G E  " + stageNumber, width/2, 16);

    this.progress();
  }

  this.progress = function(){
    if(this.px2 >= width){
      this.px2 = 0;
      changeColor();
    } else {
      this.px2 += width/(this.s*60);
    }
  }
}
