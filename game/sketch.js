var gravity;
var ball;
var bar;
var maptxt;
var map;
var restartButton;
var startButton;
var holeWidth = 25;
var menu;
var difficulty;
var difficultyName;
var currentlyPlaying = false;
var progressBar;
var inGameMoneyBar;
var money = 18;
var currentTrail;
var currentHat;

let font,
  fontsize = 32;

function preload() {
  font = loadFont('assets/SIMPLIFICA.ttf');
}

function setup() {
  textFont(font);
  textSize(fontsize);
  var canvas = createCanvas(500, 500);
  canvas.parent("game-container");
  gravity = 0.4;
  menu = new Menu();
  menu.setup();
  map = new Map();
  currentTrail = RedTrail;
  currentHat = NoHat;
  loadCoins();
  $("#outter-coin-count").html(money);
  storeLoad();
  storeDraw();
  tg.setup();
}

function draw() {
  tg.draw();

  if(currentlyPlaying){
    //draw background (color)
    map.draw();

    //draw bar
    bar.draw();

    //draw ball
    ball.draw();

    inGameMoneyBar.draw();

    progressBar.draw();
  } else {
    menu.draw();
  }
}

function start(){
  map.setup();
  ball = new Ball(width/2, 500, currentTrail, currentHat);
  bar = new Bar(height - 105, height - 105);
  progressBar = new ProgressBar();
  inGameMoneyBar = new InGameMoneyBar();
  currentlyPlaying = true;
  disableButtons();
  changeColor();
}

function end(){
  menu.setup();
  map.end();
  currentlyPlaying = false;
  levelUp();
  resetColor();
  saveCoins();
}

function mouseClicked() {
  if (!currentlyPlaying && startButton.mouseInside()) {
    theThingThatLikeMenuYouKnow();
    expand();
  }
}

$(document).on("keypress", function (e) {
  e.preventDefault();
  if (!currentlyPlaying && (e.which == 32 || e.which == 13 || e.which == 65)){
    theThingThatLikeMenuYouKnow();
    expand();
  }
  if (currentlyPlaying && e.keyCode == 27){
    end();
    expand();
  }
});

$(document).keyup(function (e) {
  if (currentlyPlaying && e.keyCode == 27){
    end();
    expand();
  }
});

function theThingThatLikeMenuYouKnow(){
  start();
  $("#movement-instruction").css("display", "none");
}
