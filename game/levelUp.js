function levelUp(){
  var difficulties = ["easy", "normal", "hard", "extreme"];
  var currentDifficulty = difficultyName.toLowerCase().replaceAll(" ", "");
  var indexCurrentDifficulty = difficulties.findIndex(function(element) {
    return element === currentDifficulty;
  });
  var nextDifficulty = difficulties[indexCurrentDifficulty + 1];
  var currentText = $("#" + currentDifficulty + "-score").html();
  var oldStage = Number(currentText.substr(12, currentText.length - 12));
  if(stageNumber > oldStage){
    $("#" + currentDifficulty + "-score").html("Best: Stage " + stageNumber);
    localStorage.setItem(currentDifficulty, "Best: Stage " + stageNumber);
    if(stageNumber >= 5 && oldStage < 5 && (currentDifficulty == "normal" || currentDifficulty == "hard")){
      $("#" + nextDifficulty).removeClass("locked");
      $("#" + nextDifficulty + " p").html(nextDifficulty.toUpperCase());
      $("#" + nextDifficulty + " span").html("Best: Stage 0");
      document.getElementById(nextDifficulty).onclick = window[nextDifficulty];
    }
  }
}

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.split(search).join(replacement);
};
