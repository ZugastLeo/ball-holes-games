function Coin(x, y){
  this.x = x;
  this.y = y;
  this.speed = 1.5;
  this.radius = holeWidth;
  this.rotationAngle = 0;
  this.trail = [];
  this.hasBeenCollected = false;

  this.draw = function(){
    if(!this.hasBeenCollected){
      this.drawTrail();
      this.drawTriangle();
      this.getRich();
    }
    this.fall();
  }

  this.drawTriangle = function(){
    this.rotationAngle++;
    var x1 = (this.x + this.radius*Math.cos(toRadians(90+this.rotationAngle)));
    var y1 = (this.y - this.radius*Math.sin(toRadians(90+this.rotationAngle)));

    var x2 = (this.x + this.radius*Math.cos(toRadians(210+this.rotationAngle)));
    var y2 = (this.y - this.radius*Math.sin(toRadians(210+this.rotationAngle)));

    var x3 = (this.x + this.radius*Math.cos(toRadians(330+this.rotationAngle)));
    var y3 = (this.y - this.radius*Math.sin(toRadians(330+this.rotationAngle)));

    fill(color(255, 215, 0));
    triangle(x2, y2, x1, y1, x3, y3);
  }

  this.trailCounter = 0;
  this.drawTrail = function(){
    if(this.trailCounter % 10 == 0){
      this.trail.push([this.x+(Math.random()*this.radius*1.7-this.radius*0.85), this.y]);
    }
    this.trailCounter++;
    for(var i = this.trail.length - 1; i >= 0 ; i--){
      if(this.y - 100 > this.trail[i][1]){
        this.trail.splice(i, 1);
      } else {
        var r = 2.5;
        var c = color((255 - map.greyScale)*((i)/this.trail.length) + map.greyScale,
                      (215 - map.greyScale)*((i)/this.trail.length) + map.greyScale,
                      (0 - map.greyScale)*((i)/this.trail.length) + map.greyScale);
        fill(c);
        noStroke()
        var x1 = (this.trail[i][0] + r*Math.cos(toRadians(90+this.rotationAngle)));
        var y1 = (this.trail[i][1] - r*Math.sin(toRadians(90+this.rotationAngle)));

        var x2 = (this.trail[i][0] + r*Math.cos(toRadians(210+this.rotationAngle)));
        var y2 = (this.trail[i][1] - r*Math.sin(toRadians(210+this.rotationAngle)));

        var x3 = (this.trail[i][0] + r*Math.cos(toRadians(330+this.rotationAngle)));
        var y3 = (this.trail[i][1] - r*Math.sin(toRadians(330+this.rotationAngle)));

        triangle(x2, y2, x1, y1, x3, y3);
      }
    }
  }

  this.fall = function(){
    this.y += this.speed;
  }

  this.isInside = function(){
    if(ball != null && dist(this.x, this.y, ball.x, ball.y) < this.radius+5){
      return true;
    }
    return false;
  }

  this.getRich = function(){
    if(this.isInside() && !this.hasBeenCollected){
      money++;
      this.radius = 0;
      this.hasBeenCollected = true;
    }
  }
}

function saveCoins(){
  localStorage.setItem("coins", money);
  $("#outter-coin-count").html(money);
}

function loadCoins(){
  if(localStorage.getItem("coins")){
    money = localStorage.getItem("coins");
    $("#outter-coin-count").html(money);
  }
}
