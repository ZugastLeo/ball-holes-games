function RedTrail(ball){
  this.trail = [];
  this.ball = ball;

  this.draw = function(){

    this.trail.push([this.ball.x, this.ball.y]);
    for(var i = this.trail.length-1; i >= 0; i--){
      var r = this.ball.radius * (1 - 0.01 * (this.trail.length-i));
      if(r <= this.ball.radius * 0.15){
        this.trail.splice(i, 1);
      } else {
        noStroke()
        fill(color(244, 80+1.9*(this.trail.length-i), 66));
        ellipse(this.trail[i][0], this.trail[i][1] + (this.trail.length-i), 2*r, 2*r);
      }
    }
    var c = color(244, 80, 66, 100);
    fill(c);
    stroke(color(200,200,200));
    strokeWeight(1);
    ellipse(this.ball.x,this.ball.y,this.ball.radius*2,this.ball.radius*2);
  }
}
