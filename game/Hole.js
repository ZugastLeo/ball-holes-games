/*
hole object
represents the holes in the game

x: horizontal position
y: vertical position
*/
function Hole(x,y){
  this.x = x;
  this.y = y;
  this.speed = 1.5;
  this.radius = holeWidth;
  this.trail = [];

  this.red = map.hred;//16;
  this.green = map.hgreen//112;
  this.blue = map.hblue//209;
  /*
  draw method runs every frame
  handles drawing on screen and killing the player
  */
  this.draw = function(){
    this.died();
    this.fall();
    this.drawTrail();

    var c = color(this.red, this.green, this.blue);
    fill(c);
    noStroke()
    ellipse(this.x,this.y, this.radius * 2,this.radius * 2);

    /*
    grey -= 15;
    var grey = 255;
    for(var i = 1; i < 8; i++){
      //(7, 13, 38)
      c = color(7*(grey/255), 13*(grey/255), 38*(grey/255));
      fill(c);
      ellipse(this.x,this.y,(this.radius * ((8-i)/8))*2,(this.radius * ((8-i)/8))*2);
      grey -= 15;
    }
*/
  }

  this.trailCounter = 0;
  this.drawTrail = function(){
    if(this.trailCounter % 10 == 0){
      this.trail.push([this.x+(Math.random()*this.radius*2-this.radius), this.y]);
    }
    this.trailCounter++;
    for(var i = this.trail.length - 1; i >= 0 ; i--){
      if(this.y - 100 > this.trail[i][1]){
        this.trail.splice(i, 1);
      } else {
        var r = 5;
        var c = color((this.red - map.greyScale)*((i)/this.trail.length) + map.greyScale,
                      (this.green - map.greyScale)*((i)/this.trail.length) + map.greyScale,
                      (this.blue - map.greyScale)*((i)/this.trail.length) + map.greyScale);
        fill(c);
        noStroke()
        ellipse(this.trail[i][0], this.trail[i][1], r, r);
      }
    }
  }

  this.died = function(){
    if(this.isInside()){
      end();
      expand();
    }
  }

  /*
  checks if the given ball is inside the hole
  */
  this.isInside = function(){
    if(ball != null && dist(this.x, this.y, ball.x, ball.y) < this.radius){
      return true;
    }
    return false;
  }

  this.fall = function(){
    this.y += this.speed;
  }
}
