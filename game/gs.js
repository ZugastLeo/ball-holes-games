$(function(){
  $("#movement-instruction").css("transition", "border-color .3s, height .2s");
  $("#movement-instruction div").css("transition", "background-color .3s");
  $(".difficulty-button").css("transition", "background-color .3s");
  $("#game-container ").css("transition", "border-color.3s");
  $(".sliding-panel-left").css("transition", "border-color .3s, left .3s");
  $(".sliding-panel-button-left").css("transition", "border-color .3s, background-color .3s");
  $(".sliding-panel-right").css("transition", "border-color .3s, right .3s");
  $(".sliding-panel-button-right").css("transition", "border-color .3s, background-color .3s");

  difficulty = 100;
  difficultyName = "N O R M A L";

  $('button').focus(function() {
       this.blur();
  });
  expand();

  if (localStorage.getItem("easy")){
    $("#" + "easy" + " span").html(localStorage.getItem("easy"));
  }
  document.getElementById("easy").onclick = easy;

  if (localStorage.getItem("normal")){
    $("#" + "normal" + " span").html(localStorage.getItem("normal"));
  }
  document.getElementById("normal").onclick = normal;

  if (localStorage.getItem("hard")){
    $("#" + "hard").removeClass("locked");
    $("#" + "hard" + " p").html("HARD");
    $("#" + "hard" + " span").html(localStorage.getItem("hard"));
    document.getElementById("hard").onclick = hard;
  }
  if (localStorage.getItem("extreme")){
    $("#" + "extreme").removeClass("locked");
    $("#" + "extreme" + " p").html("EXTREME");
    $("#" + "extreme" + " span").html(localStorage.getItem("extreme"));
    document.getElementById("extreme").onclick = extreme;
  }

  $("#store").click(clickStoreItem);

  makeCoolText("#game h1", "GALACTIC SNAKE");
});

function easy(){
  end();
  difficulty = 250;
  difficultyName = "E A S Y";
}

function normal(){
  end();
  difficulty = 100;
  difficultyName = "N O R M A L";
}

function hard(){
  end();
  difficulty = 70;
  difficultyName = "H A R D";
}

function extreme(){
  end();
  difficulty = 30;
  difficultyName = "E X T R E M E";
}

var controlOut = false;
function controls(){
  $("#movement-instruction").css("height", controlOut ? "0px" : "100px");
  $("#controls-arrow-left").css("transform", controlOut ? "rotate(-90deg)" : "rotate(90deg)");
  $("#controls-arrow-right").css("transform", controlOut ? "rotate(-90deg)" : "rotate(90deg)");
  controlOut = !controlOut;
}

var rightOut = false;
function instructions(){
  $(".sliding-panel-right").css("right", rightOut ? "0px" : "-300px");
  $("#instructions-arrow-top").css("transform", rightOut ? "rotate(0deg)" : "rotate(180deg)");
  $("#instructions-arrow-bottom").css("transform", rightOut ? "rotate(0deg)" : "rotate(180deg)");
  rightOut = !rightOut;
}

var leftOut = false;
function levels(){
  $(".sliding-panel-left").css("left", leftOut ? "0px" : "-300px");
  $("#levels-arrow-top").css("transform", leftOut ? "rotate(180deg)" : "rotate(0deg)");
  $("#levels-arrow-bottom").css("transform", leftOut ? "rotate(180deg)" : "rotate(0deg)");
  leftOut = !leftOut;
}

function expand(){
  $("#movement-instruction").css("transition", "height 1.5s");
  $("#controls-arrow-left").css("transition", "left 1s");
  $("#controls-arrow-right").css("transition", "right 1.5s");
  levels();
  instructions();
  controls()
  $("#movement-instruction").css("transition", "height .3s");
  $("#controls-arrow-left").css("transition", "left .2s");
  $("#controls-arrow-right").css("transition", "right .3s");
}

function toRadians(angle) {
  return angle * Math.PI/180;
}

var gravity;
var ball;
var bar;
var maptxt;
var map;
var restartButton;
var startButton;
var holeWidth = 25;
var menu;
var difficulty;
var difficultyName;
var currentlyPlaying = false;
var progressBar;
var inGameMoneyBar;
var money = 18;
var currentTrail;
var currentHat;

let font,
  fontsize = 32;

function preload() {
  font = loadFont('assets/SIMPLIFICA.ttf');
}

function setup() {
  textFont(font);
  textSize(fontsize);
  var canvas = createCanvas(500, 500);
  canvas.parent("game-container");
  gravity = 0.4;
  menu = new Menu();
  menu.setup();
  map = new Map();
  currentTrail = RedTrail;
  currentHat = NoHat;
  loadCoins();
  $("#outter-coin-count").html(money);
  storeLoad();
  storeDraw();
  tg.setup();
}

function draw() {
  tg.draw();

  if(currentlyPlaying){
    map.draw();
    bar.draw();
    ball.draw();
    inGameMoneyBar.draw();
    progressBar.draw();
  } else {
    menu.draw();
  }
}

function start(){
  map.setup();
  ball = new Ball(width/2, 500, currentTrail, currentHat);
  bar = new Bar(height - 105, height - 105);
  progressBar = new ProgressBar();
  inGameMoneyBar = new InGameMoneyBar();
  currentlyPlaying = true;
  changeColor();
}

function end(){
  menu.setup();
  map.end();
  currentlyPlaying = false;
  levelUp();
  resetColor();
  saveCoins();
}

function mouseClicked() {
  if (!currentlyPlaying && startButton.mouseInside()) {
    theThingThatLikeMenuYouKnow();
    expand();
  }
}

$(document).on("keypress", function (e) {
  e.preventDefault();
  if (!currentlyPlaying && (e.which == 32 || e.which == 13 || e.which == 65)){
    theThingThatLikeMenuYouKnow();
    expand();
  }
  if (currentlyPlaying && e.keyCode == 27){
    end();
    expand();
  }
});

$(document).keyup(function (e) {
  if (currentlyPlaying && e.keyCode == 27){
    end();
    expand();
  }
});

function theThingThatLikeMenuYouKnow(){
  start();
  $("#movement-instruction").css("display", "none");
}

function Ball(x, y, trail, hat){
  this.x = x;
  this.y = y;
  this.xSpeed = 0;
  this.ySpeed = 0;
  this.radius = 20;
  this.trail = new trail(this);
  this.hat = new hat(this);

  this.draw = function(){
    this.fall();
    this.sideMove();
    this.trail.draw();
    this.hat.draw();
  }

  this.fall = function(){
    this.y += this.ySpeed;
    this.ySpeed += gravity;
    if (!bar.aboveBar(this)){
      this.y = bar.f(this.x) - this.radius;
    }
  }

  this.sideMove = function(){
    this.x += this.xSpeed;
    var theta = Math.atan(bar.m());
    this.xSpeed += gravity * Math.sin(theta);

    if (this.x < bar.x1 + this.radius){
      this.x = bar.x1 + this.radius;
      this.xSpeed = 0;
    } else if (this.x > bar.x2 - this.radius){
      this.x = bar.x2 - this.radius;
      this.xSpeed = 0;
    }
  }

  this.jump = function(){
    this.ySpeed = -20;
  }
}

function RedTrail(ball){
  this.trail = [];
  this.ball = ball;

  this.draw = function(){

    this.trail.push([this.ball.x, this.ball.y]);
    for(var i = this.trail.length-1; i >= 0; i--){
      var r = this.ball.radius * (1 - 0.01 * (this.trail.length-i));
      if(r <= this.ball.radius * 0.15){
        this.trail.splice(i, 1);
      } else {
        noStroke();
        fill(color(244, 80+1.9*(this.trail.length-i), 66));
        ellipse(this.trail[i][0], this.trail[i][1] + (this.trail.length-i), 2*r, 2*r);
      }
    }
    var c = color(244, 80, 66, 100);
    fill(c);
    stroke(color(200,200,200));
    strokeWeight(1);
    ellipse(this.ball.x,this.ball.y,this.ball.radius*2,this.ball.radius*2);
  }
}

function GreenTrail(ball){
  this.trail = [];
  this.ball = ball;

  this.r1 = 25;
  this.g1 = 163;
  this.b1 = 255;

  this.r2 = 69;
  this.g2 = 244;
  this.b2 = 66;

  this.draw = function(){
    for(var i = 0; i < this.trail.length; i++){
      var radius = this.ball.radius * (1 - 0.01 * (this.trail.length - i));
      if(radius <= this.ball.radius * 0.15){
        this.trail.splice(i, 1);
      } else {
        noStroke();
        var r = this.r2 + (this.r1 - this.r2) * (i/this.trail.length);
        var g = this.g2 + (this.g1 - this.g2) * (i/this.trail.length);
        var b = this.b2 + (this.b1 - this.b2) * (i/this.trail.length);
        fill(color(r, g, b));
        ellipse(this.trail[i][0], this.trail[i][1] + (this.trail.length - i), 2*radius, 2*radius);
      }
    }
    var c = color(this.r1, this.g1, this.b1);
    fill(c);
    stroke(color(200,200,200));
    strokeWeight(1);
    ellipse(this.ball.x,this.ball.y,this.ball.radius*2,this.ball.radius*2);

    this.trail.push([this.ball.x, this.ball.y]);
  }
}

function RainbowTrail(ball){
  this.trail = [];
  this.ball = ball;

  var center = 128;
  var width = 127;
  var phase = 100;

  this.draw = function(){
    var frequency = Math.PI*2/this.trail.length;

    var r;
    var g;
    var b;

    for(var i = 0; i < this.trail.length; i++){
      var radius = this.ball.radius * (1 - 0.01 * (this.trail.length - i));
      if(radius <= this.ball.radius * 0.15){
        this.trail.splice(i, 1);
      } else {
        noStroke();
        r = Math.sin(frequency*i+2+phase) * width + center;
        g = Math.sin(frequency*i+0+phase) * width + center;
        b = Math.sin(frequency*i+4+phase) * width + center;
        fill(color(r, g, b));
        ellipse(this.trail[i][0], this.trail[i][1] + (this.trail.length - i), 2*radius, 2*radius);
      }
    }
    var c = color(r, g, b);
    fill(c);
    stroke(color(200,200,200));
    strokeWeight(1);
    ellipse(this.ball.x,this.ball.y,this.ball.radius*2,this.ball.radius*2);

    phase+=0.02;

    this.trail.push([this.ball.x, this.ball.y]);
  }
}

function BlueTrail(ball){
  this.trail = [];
  this.ball = ball;

  this.r1 = 170;
  this.g1 = 25;
  this.b1 = 255;

  this.r2 = 24;
  this.g2 = 105;
  this.b2 = 255;

  this.draw = function(){
    for(var i = 0; i < this.trail.length; i++){
      var radius = this.ball.radius * (1 - 0.01 * (this.trail.length - i));
      if(radius <= this.ball.radius * 0.15){
        this.trail.splice(i, 1);
      } else {
        noStroke();
        var r = this.r2 + (this.r1 - this.r2) * (i/this.trail.length);
        var g = this.g2 + (this.g1 - this.g2) * (i/this.trail.length);
        var b = this.b2 + (this.b1 - this.b2) * (i/this.trail.length);
        fill(color(r, g, b));
        ellipse(this.trail[i][0], this.trail[i][1] + (this.trail.length - i), 2*radius, 2*radius);
      }
    }
    var c = color(this.r1, this.g1, this.b1);
    fill(c);
    stroke(color(200,200,200));
    strokeWeight(1);
    ellipse(this.ball.x,this.ball.y,this.ball.radius*2,this.ball.radius*2);

    this.trail.push([this.ball.x, this.ball.y]);
  }
}

function CrownHat(ball){
  this.ball = ball;

  this.draw = function(){
    var x = this.ball.x;
    var y = this.ball.y;

    fill("#FFFF00");
    rect(x-19, y-15, 37, 5);
    triangle(x-18, y-15, x-20, y-35, x-5, y-15);
    triangle(x-10, y-15, x, y-35, x+10, y-15);
    triangle(x+5, y-15, x+20, y-35, x+18, y-15);
  }
}

function CowboyHat(ball){
  this.ball = ball;

  this.draw = function(){
    var x = this.ball.x;
    var y = this.ball.y;

    noFill();
    stroke("#966a04");
    strokeWeight(5);
    arc(x, y-15, 40, 5, 0, PI);
    line(x-10, y-15, x-5, y-30);
    line(x+10, y-15, x+5, y-30);
    arc(x+.5, y-29.5, 10, 5, 0, PI);

    line(x-5, y-15, x-3, y-25);
    line(x+5, y-15, x+3, y-25);
    line(x-2, y-15, x, y-25);
    line(x+2, y-15, x, y-25);

    stroke("#4c3500");
    arc(x+.5, y-18, 19, 1, 0, PI);

    fill("#4c3500");
    strokeWeight(2);
    line(x-10.6, y-16.5, x-9.6, y-19.5);
    line(x+11.8, y-16.5, x+10.8, y-19.5);
    line(x+11.9, y-16.5, x-10.7, y-16.5);
  }
}

function AngelHat(ball){
  this.ball = ball;

  this.draw = function(){
    var x = this.ball.x;
    var y = this.ball.y;

    noFill();
    stroke("#F0E68C");
    strokeWeight(3);
    ellipse(x, y-20, 35, 8);
  }
}

function NoHat(){
  this.draw = function(){

  }
}

function Bar(y1, y2){
  this.x1 = 0;
  this.y1 = y1;
  this.x2 = width;
  this.y2 = y2;

  this.tr = [15, 17, 22, 10, 8, 10, color(255,255,255)];
  this.br = [15, 17, 22, 10, 8, 10, color(255,255,255)];
  this.tl = [15, 17, 22, 10, 8, 10, color(255,255,255)];
  this.bl = [15, 17, 22, 10, 8, 10, color(255,255,255)];

  this.draw = function(){
    this.tilt();

    strokeWeight(4);
    stroke(color(255,255,255));
    line(this.x1, this.y1, this.x2, this.y2);

    noStroke();
    fill(this.tr[6]);
    triangle(this.x2-this.tr[0], this.y2-this.tr[1], this.x2-this.tr[2], this.y2-this.tr[3], this.x2-this.tr[4], this.y2-this.tr[5]);

    fill(this.br[6]);
    triangle(this.x2-this.br[0], this.y2+this.br[1], this.x2-this.br[2], this.y2+this.br[3], this.x2-this.br[4], this.y2+this.br[5]);

    fill(this.tl[6]);
    triangle(this.x1+this.tl[0], this.y1-this.tl[1], this.x1+this.tl[2], this.y1-this.tl[3], this.x1+this.tl[4], this.y1-this.tl[5]);

    fill(this.bl[6]);
    triangle(this.x1+this.bl[0], this.y1+this.bl[1], this.x1+this.bl[2], this.y1+this.bl[3], this.x1+this.bl[4], this.y1+this.bl[5]);
  }

  this.m = function(){
    return (this.y2 - this.y1)/(this.x2 - this.x1);
  }

  this.f = function(x){
    return this.m() * (x - this.x1) + this.y1;
  }

  this.aboveBar = function(){
    if(ball.y + ball.radius < this.f(ball.x)){
      return true;
    } else {
      return false;
    }
  }

  this.tilt = function(){
    this.tl = [15, 17, 22, 10, 8, 10, color(255,255,255)];
    this.bl = [15, 17, 22, 10, 8, 10, color(255,255,255)];
    if(keyIsDown(65)){
      this.y1 += -5;
      if(this.y1 < 0){
        this.y1 = 0;
      }

      this.tl = [15, 21, 26, 10, 4, 10, color(255,0,0)];
    } else if (keyIsDown(90)){
      this.y1 += 5;
      if(this.y1 > height){
        this.y1 = height;
      }

      this.bl = [15, 21, 26, 10, 4, 10, color(255,0,0)];
    }

    this.tr = [15, 17, 22, 10, 8, 10, color(255,255,255)];
    this.br = [15, 17, 22, 10, 8, 10, color(255,255,255)];
    if (keyIsDown(75)){
      this.y2 += -5;
      if(this.y2 < 0){
        this.y2 = 0;
      }

      this.tr = [15, 21, 26, 10, 4, 10, color(255,0,0)];
    } else if (keyIsDown(77)){
      this.y2 += 5;
      if(this.y2 > height){
        this.y2 = height;
      }
      this.br = [15, 21, 26, 10, 4, 10, color(255,0,0)];
    }
  }
}

function Hole(x,y){
  this.x = x;
  this.y = y;
  this.speed = 1.5;
  this.radius = holeWidth;
  this.trail = [];

  this.red = map.hred;
  this.green = map.hgreen;
  this.blue = map.hblue;

  this.draw = function(){
    this.died();
    this.fall();
    this.drawTrail();

    var c = color(this.red, this.green, this.blue);
    fill(c);
    noStroke();
    ellipse(this.x,this.y, this.radius * 2,this.radius * 2);
  }

  this.trailCounter = 0;
  this.drawTrail = function(){
    if(this.trailCounter % 10 == 0){
      this.trail.push([this.x+(Math.random()*this.radius*2-this.radius), this.y]);
    }
    this.trailCounter++;
    for(var i = this.trail.length - 1; i >= 0 ; i--){
      if(this.y - 100 > this.trail[i][1]){
        this.trail.splice(i, 1);
      } else {
        var r = 5;
        var c = color((this.red - map.greyScale)*((i)/this.trail.length) + map.greyScale,
                      (this.green - map.greyScale)*((i)/this.trail.length) + map.greyScale,
                      (this.blue - map.greyScale)*((i)/this.trail.length) + map.greyScale);
        fill(c);
        noStroke();
        ellipse(this.trail[i][0], this.trail[i][1], r, r);
      }
    }
  }

  this.died = function(){
    if(this.isInside()){
      end();
      expand();
    }
  }

  this.isInside = function(){
    if(ball != null && dist(this.x, this.y, ball.x, ball.y) < this.radius){
      return true;
    }
    return false;
  }

  this.fall = function(){
    this.y += this.speed;
  }
}

function Coin(x, y){
  this.x = x;
  this.y = y;
  this.speed = 1.5;
  this.radius = holeWidth;
  this.rotationAngle = 0;
  this.trail = [];
  this.hasBeenCollected = false;

  this.draw = function(){
    if(!this.hasBeenCollected){
      this.drawTrail();
      this.drawTriangle();
      this.getRich();
    }
    this.fall();
  }

  this.drawTriangle = function(){
    this.rotationAngle++;
    var x1 = (this.x + this.radius*Math.cos(toRadians(90+this.rotationAngle)));
    var y1 = (this.y - this.radius*Math.sin(toRadians(90+this.rotationAngle)));

    var x2 = (this.x + this.radius*Math.cos(toRadians(210+this.rotationAngle)));
    var y2 = (this.y - this.radius*Math.sin(toRadians(210+this.rotationAngle)));

    var x3 = (this.x + this.radius*Math.cos(toRadians(330+this.rotationAngle)));
    var y3 = (this.y - this.radius*Math.sin(toRadians(330+this.rotationAngle)));

    fill(color(255, 215, 0));
    triangle(x2, y2, x1, y1, x3, y3);
  }

  this.trailCounter = 0;
  this.drawTrail = function(){
    if(this.trailCounter % 10 == 0){
      this.trail.push([this.x+(Math.random()*this.radius*1.7-this.radius*0.85), this.y]);
    }
    this.trailCounter++;
    for(var i = this.trail.length - 1; i >= 0 ; i--){
      if(this.y - 100 > this.trail[i][1]){
        this.trail.splice(i, 1);
      } else {
        var r = 2.5;
        var c = color((255 - map.greyScale)*((i)/this.trail.length) + map.greyScale,
                      (215 - map.greyScale)*((i)/this.trail.length) + map.greyScale,
                      (0 - map.greyScale)*((i)/this.trail.length) + map.greyScale);
        fill(c);
        noStroke();
        var x1 = (this.trail[i][0] + r*Math.cos(toRadians(90+this.rotationAngle)));
        var y1 = (this.trail[i][1] - r*Math.sin(toRadians(90+this.rotationAngle)));

        var x2 = (this.trail[i][0] + r*Math.cos(toRadians(210+this.rotationAngle)));
        var y2 = (this.trail[i][1] - r*Math.sin(toRadians(210+this.rotationAngle)));

        var x3 = (this.trail[i][0] + r*Math.cos(toRadians(330+this.rotationAngle)));
        var y3 = (this.trail[i][1] - r*Math.sin(toRadians(330+this.rotationAngle)));

        triangle(x2, y2, x1, y1, x3, y3);
      }
    }
  }

  this.fall = function(){
    this.y += this.speed;
  }

  this.isInside = function(){
    if(ball != null && dist(this.x, this.y, ball.x, ball.y) < this.radius+5){
      return true;
    }
    return false;
  }

  this.getRich = function(){
    if(this.isInside() && !this.hasBeenCollected){
      money++;
      this.radius = 0;
      this.hasBeenCollected = true;
    }
  }
}

function saveCoins(){
  localStorage.setItem("coins", money);
  $("#outter-coin-count").html(money);
}

function loadCoins(){
  if(localStorage.getItem("coins")){
    money = localStorage.getItem("coins");
    $("#outter-coin-count").html(money);
  }
}

function Map(map){
  this.holes = [];
  this.spawners = [];
  this.greyScale = 15;

  this.hred = 16;
  this.hgreen = 112;
  this.hblue = 209;

  this.setup = function(){
    var numSpawners = width/(holeWidth*2)+1;
    for(var i = 0; i < numSpawners; i++){
      this.spawners[i] = new Spawner(i*holeWidth*2, -holeWidth*2, difficulty);
      this.spawners[i].setup(this.spawners[i]);
    }
  }

  this.draw = function(){
    background(color(this.greyScale,this.greyScale,this.greyScale)/*color(222,184,135)*/);

    for(var i = 0; i < this.spawners.length; i++){
      this.spawners[i].draw();
    }

    for(var i = 1; i < this.holes.length; i++){
      if(this.holes[i].y > height+holeWidth+5){
        this.holes.splice(i, 1);
      } else {
        this.holes[i].draw();
      }
    }
  }

  this.end = function(){
    this.holes = [];
    this.spawners = [];
  }
}

function Button(w,h,t){
  this.w = w*2;
  this.h = h*2;
  this.x = width/2-this.w/2;
  this.y = height/2-this.h/2;
  this.text = t;

  this.draw = function(){
    noStroke();
    strokeWeight(2);
    fill(color(245,245,245));
    rect(width/2-this.w/2, height/2-this.h/2, this.w, this.h, 10);

    textSize(40);
    textAlign(CENTER);
    fill(0, 102, 153);
    text(this.text, width/2, height/2+14);
    textAlign(LEFT);
  }

  this.mouseInside = function(){
    return this.x < mouseX && this.x + this.w > mouseX && this.y < mouseY && this.y + this.h > mouseY;
  }
}

function Spawner(x,y,t){
  this.x = x;
  this.y = y;
  this.t = t;

  this.draw = function(){
    this.timer.tick();
  }

  this.setup = function(spawner){
    spawner.ms = Math.floor(Math.random() * spawner.t+10);
    spawner.timer = new Timer(function(){spawner.spawn(spawner);}, 10*spawner.ms);
  }

  this.spawn = function(spawner){
    var isCoin = (Math.random() * 10 - 8.5) >= 0;
    map.holes.push(isCoin ? new Coin(spawner.x, spawner.y) : new Hole(spawner.x, spawner.y));
    spawner.setup(spawner);
  }
}

function Timer(f, s){
  this.s = s;
  this.f = f;
  this.frames = 0;
  this.tick = function(){
    this.frames++;
    if(this.frames % this.s == 0){
      this.f();
    }
  }
}

function Menu(){
  this.setup = function(){
    startButton = new Button(70,25,"S T A R T");
    $("#movement-instruction").css("display", "block");
  }

  this.draw = function(){
    noStroke();
    fill(color(50,50,50));
    rect(0,0,width,height);
    startButton.draw();

    textSize(70);
    fill(color(245,245,245));
    textAlign(CENTER);
    text(difficultyName, width/2, 100);

    textSize(18);
    fill(color(245,245,245));
    noStroke();
    textAlign(CENTER);
    text("( S P A C E B A R )  O R  ( E N T E R )", width/2, 300);
  }
}

var store = {
    //Trails
    "orange-trail" : {
      fcn: RedTrail,
      state: "selected",
      price: 0,
      type: "trail"
    },

    "green-trail" : {
      fcn: GreenTrail,
      state: "locked",
      price: 5,
      type: "trail"
    },

    "purple-trail" : {
      fcn: BlueTrail,
      state: "locked",
      price: 10,
      type: "trail"
    },

    "rainbow-trail" : {
      fcn: RainbowTrail,
      state: "locked",
      price: 20,
      type: "trail"
    },

    //Hats
    "no-hat" : {
      fcn: NoHat,
      state: "selected",
      price: 0,
      type: "hat"
    },

    "angel-hat" : {
      fcn: AngelHat,
      state: "locked",
      price: 5,
      type: "hat"
    },

    "cowboy-hat" : {
      fcn: CowboyHat,
      state: "locked",
      price: 10,
      type: "hat"
    },

    "crown-hat" : {
      fcn: CrownHat,
      state: "locked",
      price: 20,
      type: "hat"
    }
}

function storeLoad(){
  fakeStore = JSON.parse(localStorage.getItem("store"));
  if(fakeStore){
    for(const itemId in store){
      store[itemId].state = fakeStore[itemId].state;
    }
  }
}

function storeSave(){
  localStorage.setItem("store", JSON.stringify(store));
}

function storeDraw(){
  function stateLoad(itemId){
    $("#" + itemId).removeClass("locked");
    $("#" + itemId).removeClass("unlocked");
    $("#" + itemId).removeClass("selected");
    switch(store[itemId].state){
      case "locked":
        $("#" + itemId).addClass("locked");
        $("#" + itemId + " .price").html(store[itemId].price);
        break;
      case "unlocked":
        $("#" + itemId).addClass("unlocked");
        $("#" + itemId + " .price").html(" - ");
        break;
      case "selected":
        $("#" + itemId).addClass("selected");
        $("#" + itemId + " .price").html(" ✔ ");
        if(("" + itemId).includes("trail")){
          currentTrail = store[itemId].fcn;
        } else {
          currentHat = store[itemId].fcn;
        }
        break;
    }
  }

  for(const itemId in store){
    stateLoad(itemId);
  }
}

function clickStoreItem(e){
  e = e || window.event;
  e = e.target || e.srcElement;
  classes = e.classList;
  if (e.nodeName === "BUTTON") {
    if (isThisTypeOfItem(classes, "trail")){
      if(store[e.id].state != "locked"){
        changeItem(e.id, "trail");
      } else {
        var successful = purchaseItem(e.id);
        if(successful){
          changeItem(e.id, "trail");
        }
      }
    } else if (isThisTypeOfItem(classes, "hat")){
        if(store[e.id].state != "locked"){
          changeItem(e.id, "hat");
        } else {
          var successful = purchaseItem(e.id);
          if(successful){
            changeItem(e.id, "hat");
          }
        }
    }
  }
  saveCoins();
  storeSave();
  storeLoad();
  storeDraw();
}

function purchaseItem(itemId){
  if(store[itemId].price > money){
    notEnoughMoney();
    return false;
  } else {
    money-=store[itemId].price;
    changeItemState(itemId, 2);
    return true;
  }
}

function notEnoughMoney(){
  alert("You do not have enough money for this item");
}

function changeItem(itemId, itemType){
  for(const item in store){
    if(store[item].state == "selected" && store[item].type == itemType){
      store[item].state = "unlocked";
    }
  }
  store[itemId].state = "selected";
}

function changeItemState(itemId, state){
  store[itemId].state = state;
}

function isThisTypeOfItem(classes, itemType){
  for(var i = 0; i < classes.length; i++){
    if(classes[i] == itemType + "-button"){
      return true;
    }
  }
  return false
}

var changeColorInterval;
var colorIndex = -1;
var colors = ["#1070d1", "#00FF00", "#FF0000", "#FF00FF"];
var defaultColor = "#1070d1";
var colorTimer;
var stageNumber = 0;

function changeColor(){
    colorIndex++;
    if(colorIndex == colors.length){
      colorIndex = 0;
    }

    $("#game-container ").css("border-color", colors[colorIndex]);
    $(".sliding-panel-button-left").css({"border-color":colors[colorIndex] , "background-color" : colors[colorIndex]});
    $(".sliding-panel-button-right").css({"border-color":colors[colorIndex] , "background-color" : colors[colorIndex]});

    map.hred = parseInt(colors[colorIndex].substr(1, 2), 16);
    map.hgreen = parseInt(colors[colorIndex].substr(3, 2), 16);
    map.hblue = parseInt(colors[colorIndex].substr(5, 2), 16);

    for(var i = 0; i < map.holes.length; i++){
      map.holes[i].red = map.hred;
      map.holes[i].green = map.hgreen;
      map.holes[i].blue = map.hblue;
    }

    stageNumber++;
}

function resetColor(){
  colorIndex = 0;

  map.hred = parseInt(colors[colorIndex].substr(1, 2), 16);
  map.hgreen = parseInt(colors[colorIndex].substr(3, 2), 16);
  map.hblue = parseInt(colors[colorIndex].substr(5, 2), 16);

  $("#game-container ").css("border-color", defaultColor);
  $(".sliding-panel-left").css("border-color", defaultColor);
  $(".sliding-panel-button-left").css({"border-color": defaultColor, "background-color" : defaultColor});
  $(".sliding-panel-button-right").css({"border-color": defaultColor, "background-color" : defaultColor});

  colorIndex = -1;
  stageNumber = 0;
}

function ProgressBar(){
  this.s = 15;

  this.x1 = 0;
  this.y1 = 0;
  this.x2 = width;
  this.y2 = 22;

  this.px1 = 0;
  this.py1 = 0;
  this.px2 = 0;
  this.py2 = 22;


  this.draw = function(){
    noStroke();

    fill(color(65, 65, 65));
    rect(this.x1, this.y1, this.x2, this.y2);

    fill(map.hred, map.hgreen, map.hblue);
    rect(this.px1, this.py1, this.px2, this.py2);

    noStroke();
    textSize(20);
    textStyle(BOLD);
    fill(color(245,245,245));
    textAlign(CENTER);
    text("S T A G E  " + stageNumber, width/2, 16);

    this.progress();
  }

  this.progress = function(){
    if(this.px2 >= width){
      this.px2 = 0;
      changeColor();
    } else {
      this.px2 += width/(this.s*60);
    }
  }
}

function InGameMoneyBar(){
  this.x1 = 220;
  this.y1 = 11;

  this.rotationAngle = 0;
  this.triangleRadius = 8;

  this.draw = function(){
    noStroke();

    fill(color(65, 65, 65));
    rect(this.x1, this.y1, 60, 32, 10);

    this.drawTriangle();

    noStroke();
    textSize(15);
    fill(color(245,245,245));
    textAlign(CENTER);
    text(money, width/2 + 12, 37);
  }

  this.drawTriangle = function(){
    this.rotationAngle++;
    var x1 = (this.x1+17 + this.triangleRadius*Math.cos(toRadians(90+this.rotationAngle)));
    var y1 = (this.y1+21 - this.triangleRadius*Math.sin(toRadians(90+this.rotationAngle)));

    var x2 = (this.x1+17 + this.triangleRadius*Math.cos(toRadians(210+this.rotationAngle)));
    var y2 = (this.y1+21 - this.triangleRadius*Math.sin(toRadians(210+this.rotationAngle)));

    var x3 = (this.x1+17 + this.triangleRadius*Math.cos(toRadians(330+this.rotationAngle)));
    var y3 = (this.y1+21 - this.triangleRadius*Math.sin(toRadians(330+this.rotationAngle)));

    fill(color(255, 215, 0));
    triangle(x2, y2, x1, y1, x3, y3);
  }
}

function levelUp(){
  var difficulties = ["easy", "normal", "hard", "extreme"];
  var currentDifficulty = difficultyName.toLowerCase().replaceAll(" ", "");
  var indexCurrentDifficulty = difficulties.findIndex(function(element) {
    return element === currentDifficulty;
  });
  var nextDifficulty = difficulties[indexCurrentDifficulty + 1];
  var currentText = $("#" + currentDifficulty + "-score").html();
  var oldStage = Number(currentText.substr(12, currentText.length - 12));
  if(stageNumber > oldStage){
    $("#" + currentDifficulty + "-score").html("Best: Stage " + stageNumber);
    localStorage.setItem(currentDifficulty, "Best: Stage " + stageNumber);
    if(stageNumber >= 5 && oldStage < 5 && (currentDifficulty == "normal" || currentDifficulty == "hard")){
      $("#" + nextDifficulty).removeClass("locked");
      $("#" + nextDifficulty + " p").html(nextDifficulty.toUpperCase());
      $("#" + nextDifficulty + " span").html("Best: Stage 0");
      document.getElementById(nextDifficulty).onclick = window[nextDifficulty];
    }
  }
}

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.split(search).join(replacement);
};

function makeCoolText(elementIdentifier, message){
  $(elementIdentifier).lettering();
  var listOfRandomChars = [':', ';', '}', '{', '(', ')', '[', ']', '?', '!', '>', '<', '@', '#', '$', '%', '&'];
  var currentMessage =  "";
  for(var i = 0; i < message.length; i++){
    var randomChar = Math.floor(Math.random() * listOfRandomChars.length);
    currentMessage += listOfRandomChars[randomChar];
  }

  var currentIndex = 0;
  var switchNumber = 0;
  var maxSwitch = 5;
  var interval = setInterval(function(){
    var randomChar2 = Math.floor(Math.random() * listOfRandomChars.length);
    currentMessage = currentMessage.replaceAt(currentIndex, listOfRandomChars[randomChar2]);
    switchNumber++;
    if(switchNumber == maxSwitch){
      currentMessage = currentMessage.replaceAt(currentIndex, message.charAt(currentIndex));
      switchNumber = 0;
      currentIndex++;
      if(currentIndex == message.length){
        clearInterval(interval);
      }
    }
    $(elementIdentifier).html(currentMessage);
    $(elementIdentifier).lettering();
  }, 50);
}

String.prototype.replaceAt=function(index, replacement) {
    return this.substr(0, index) + replacement+ this.substr(index + replacement.length);
}

var tg = {
  tg1: {},
  tg2: {},

  trail1: {},
  trail2: {},

  draw: function(){
    tg1.background("#151515");
    tg2.background("#151515");
    trail1.draw();
    trail2.draw();
  },



  setup: function(){
    tg1 = createGraphics(190,100);
    tg2 = createGraphics(190,100);

    tg1.parent("tg1");
    tg2.parent("tg2");

    trail1 = new trail(tg1.width-50, tg1.height/2, 25, tg1, -1, 3.14);
    trail2 = new trail(50, tg2.height/2, 25, tg2, 1, 0);

    $("canvas").css("display", "inherit");
  },


}

function trail(x, y, radius, tgn, direction, shift){
  this.trail = [];
  this.x = x;
  this.y = y;
  this.radius = radius;
  this.tgn = tgn;
  this.sinx = 0;
  this.shift = shift

  this.draw = function(){
    this.sinx+=0.05;
    this.trail.push([this.x, this.y + Math.sin(this.sinx + shift)*20]);
    for(var i = this.trail.length-1; i >= 0; i--){
      var r = this.radius * (1 - 0.007 * (this.trail.length-i));
      if(r <= this.radius * 0.15){
        this.trail.splice(i, 1);
      } else {
        tgn.noStroke();
        tgn.fill(color(244, 80+1.9*(this.trail.length-i), 66));
        tgn.ellipse(this.trail[i][0] + direction * (this.trail.length-i), this.trail[i][1], 2*r, 2*r);
      }
    }
  }
}
