var changeColorInterval;
var colorIndex = -1;
var colors = ["#1070d1", "#00FF00", "#FF0000", "#FF00FF"];
var defaultColor = "#1070d1";
var colorTimer;
var stageNumber = 0;

function changeColor(){
    colorIndex++;
    if(colorIndex == colors.length){
      colorIndex = 0;
    }

    $("#game-container ").css("border-color", colors[colorIndex]);
    $(".sliding-panel-button-left").css({"border-color":colors[colorIndex] , "background-color" : colors[colorIndex]});
    $(".sliding-panel-button-right").css({"border-color":colors[colorIndex] , "background-color" : colors[colorIndex]});

    map.hred = parseInt(colors[colorIndex].substr(1, 2), 16);
    map.hgreen = parseInt(colors[colorIndex].substr(3, 2), 16);
    map.hblue = parseInt(colors[colorIndex].substr(5, 2), 16);

    for(var i = 0; i < map.holes.length; i++){
      map.holes[i].red = map.hred;
      map.holes[i].green = map.hgreen;
      map.holes[i].blue = map.hblue;
    }

    stageNumber++;
}

function resetColor(){
  colorIndex = 0;

  map.hred = parseInt(colors[colorIndex].substr(1, 2), 16);
  map.hgreen = parseInt(colors[colorIndex].substr(3, 2), 16);
  map.hblue = parseInt(colors[colorIndex].substr(5, 2), 16);

  $("#game-container ").css("border-color", defaultColor);
  $(".sliding-panel-left").css("border-color", defaultColor);
  $(".sliding-panel-button-left").css({"border-color": defaultColor, "background-color" : defaultColor});
  $(".sliding-panel-button-right").css({"border-color": defaultColor, "background-color" : defaultColor});

  colorIndex = -1;
  stageNumber = 0;
}
