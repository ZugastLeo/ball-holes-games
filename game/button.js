function Button(w,h,t){
  this.w = w*2;
  this.h = h*2;
  this.x = width/2-this.w/2;
  this.y = height/2-this.h/2;
  this.text = t;

  this.draw = function(){
    noStroke();
    strokeWeight(2);
    fill(color(245,245,245));
    rect(width/2-this.w/2, height/2-this.h/2, this.w, this.h, 10);

    textSize(40);
    textAlign(CENTER);
    fill(0, 102, 153);
    text(this.text, width/2, height/2+14);
    textAlign(LEFT);
  }

  this.mouseInside = function(){
    return this.x < mouseX && this.x + this.w > mouseX && this.y < mouseY && this.y + this.h > mouseY;
  }
}
