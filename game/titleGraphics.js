var tg = {
  tg1: {},
  tg2: {},

  trail1: {},
  trail2: {},

  draw: function(){
    tg1.background("#151515");
    tg2.background("#151515");
    trail1.draw();
    trail2.draw();
  },



  setup: function(){
    tg1 = createGraphics(190,100);
    tg2 = createGraphics(190,100);

    tg1.parent("tg1");
    tg2.parent("tg2");

    trail1 = new trail(tg1.width-50, tg1.height/2, 25, tg1, -1, 3.14);
    trail2 = new trail(50, tg2.height/2, 25, tg2, 1, 0);

    $("canvas").css("display", "inherit");
  },


}

function trail(x, y, radius, tgn, direction, shift){
  this.trail = [];
  this.x = x;
  this.y = y;
  this.radius = radius;
  this.tgn = tgn;
  this.sinx = 0;
  this.shift = shift

  this.draw = function(){
    this.sinx+=0.05;
    this.trail.push([this.x, this.y + Math.sin(this.sinx + shift)*20]);
    for(var i = this.trail.length-1; i >= 0; i--){
      var r = this.radius * (1 - 0.007 * (this.trail.length-i));
      if(r <= this.radius * 0.15){
        this.trail.splice(i, 1);
      } else {
        tgn.noStroke()
        tgn.fill(color(244, 80+1.9*(this.trail.length-i), 66));
        tgn.ellipse(this.trail[i][0] + direction * (this.trail.length-i), this.trail[i][1], 2*r, 2*r);
      }
    }
  }
}
