function Timer(f, s){
  this.s = s;
  this.f = f;
  this.frames = 0;
  this.tick = function(){
    this.frames++;
    if(this.frames % this.s == 0){
      this.f();
    }
  }
}
