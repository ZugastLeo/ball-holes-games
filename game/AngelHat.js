function AngelHat(ball){
  this.ball = ball;

  this.draw = function(){
    var x = this.ball.x;
    var y = this.ball.y;

    noFill();
    stroke("#F0E68C");
    strokeWeight(3);
    ellipse(x, y-20, 35, 8);
  }
}
