function InGameMoneyBar(){
  this.x1 = 220;
  this.y1 = 11;

  this.rotationAngle = 0;
  this.triangleRadius = 8;

  this.draw = function(){
    noStroke();

    fill(color(65, 65, 65));
    rect(this.x1, this.y1, 60, 32, 10);

    this.drawTriangle();

    noStroke();
    textSize(15);
    fill(color(245,245,245));
    textAlign(CENTER);
    text(money, width/2 + 12, 37);
  }

  this.drawTriangle = function(){
    this.rotationAngle++;
    var x1 = (this.x1+17 + this.triangleRadius*Math.cos(toRadians(90+this.rotationAngle)));
    var y1 = (this.y1+21 - this.triangleRadius*Math.sin(toRadians(90+this.rotationAngle)));

    var x2 = (this.x1+17 + this.triangleRadius*Math.cos(toRadians(210+this.rotationAngle)));
    var y2 = (this.y1+21 - this.triangleRadius*Math.sin(toRadians(210+this.rotationAngle)));

    var x3 = (this.x1+17 + this.triangleRadius*Math.cos(toRadians(330+this.rotationAngle)));
    var y3 = (this.y1+21 - this.triangleRadius*Math.sin(toRadians(330+this.rotationAngle)));

    fill(color(255, 215, 0));
    triangle(x2, y2, x1, y1, x3, y3);
  }
}
