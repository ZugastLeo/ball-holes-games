function Map(map){
  this.holes = [];
  this.spawners = [];
  this.greyScale = 15;

  this.hred = 16;
  this.hgreen = 112;
  this.hblue = 209;

  this.setup = function(){
    var numSpawners = width/(holeWidth*2)+1;
    for(var i = 0; i < numSpawners; i++){
      this.spawners[i] = new Spawner(i*holeWidth*2, -holeWidth*2, difficulty);
      this.spawners[i].setup(this.spawners[i]);
    }
  }

  this.draw = function(){
    background(color(this.greyScale,this.greyScale,this.greyScale)/*color(222,184,135)*/);

    for(var i = 0; i < this.spawners.length; i++){
      this.spawners[i].draw();
    }

    for(var i = 1; i < this.holes.length; i++){
      if(this.holes[i].y > height+holeWidth+5){
        this.holes.splice(i, 1);
      } else {
        this.holes[i].draw();
      }
    }
  }

  this.end = function(){
    this.holes = [];
    this.spawners = [];
  }
}
